FROM debian:stable-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -q update \
    && apt-get install -qy --no-install-recommends pandoc texlive-xetex texlive-fonts-recommended lmodern librsvg2-bin make graphviz \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/* /var/cache/apt/*

